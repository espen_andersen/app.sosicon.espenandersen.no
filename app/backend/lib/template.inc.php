<?php

  require_once( DIR_LIB . '/factory.inc.php' );

  define( 'TEMPLATE_TAG_PATTERN', '/%%([^%]+)%%/' );
  define( 'TEMPLATE_INC_PATTERN', '/##([^#]+)##/' );
  define( 'TEMPLATE_FUNCTION_PATTERN', '/^([^\\(]+)\\(([^\\)]*)\\)$/' );

  class Template {

    private $mTplText;
    private $mSequence;
    private static $mCurrentRecord = array();

    public function __construct( $idt ) {
      if( file_exists( $idt ) ) {
        $tplFile = $idt;
      }
      else {
        $tplFile = sprintf( '%s/%s.tpl', DIR_TPL, $idt );
      }
      $this->mTplText = file_exists( $tplFile ) ? file_get_contents( $tplFile ) : '';
    }

    public function render( $data = array() ) {
      if( isset( $data[ 0 ] ) ) {
        $html = '';
        for( $this->mSequence = 0; $this->mSequence < count( $data ); $this->mSequence++ ) {
          $html .= $this->renderOne( array_merge( $data[ $this->mSequence ], array( 'sequence' => $this->mSequence ) ) );
        }
      }
      else {
        $html = $this->renderOne( $data );
      }
      return $html;
    }

    public static function getCurrentRecord() {
      return end( self::$mCurrentRecord );
    }

    public function renderOne( $data ) {
      if( empty( $this->mTplText ) ) {
        return '';
      }
      $curRec = is_array( $data ) ? $data : array();
      $curRec[ '__parent__' ] = end( self::$mCurrentRecord );
      self::$mCurrentRecord[] = $curRec;
      $resolved = array();
      // Expand template includes
      $app = Factory::getApplication();
      if( preg_match_all( TEMPLATE_INC_PATTERN, $this->mTplText, $m0, PREG_SET_ORDER ) ) {
        foreach( $m0 as $inc ) {
          if( !isset( $resolved[ $inc[ 0 ] ] ) ) {
            $resolved[ $inc[ 0 ] ] = $app->tpl( $inc[ 1 ], true );
          }
        }
      }
      // Expand value tags
      if( preg_match_all( TEMPLATE_TAG_PATTERN, $this->mTplText, $m0, PREG_SET_ORDER ) ) {
        foreach( $m0 as $tag ) {
          $subject = $tag[ 0 ];
          if( !isset( $resolved[ $subject ] ) ) {
            $expression = trim( $tag[ 1 ] );
            $a = explode( '|', $expression );
            $n = count( $a );
            $field = trim( $a[ 0 ] );
            $functions = array();
            for( $i = 1; $i < $n; $i++ ) {
              if( preg_match( TEMPLATE_FUNCTION_PATTERN, trim( $a[ $i ] ), $m1 ) ) {
                $f = trim( $m1[ 1 ] );
                $p = trim( $m1[ 2 ] );
                $functions[ $f ] = ( $p == '' ? array() : array_map( 'trim', preg_split( "/\\'\\s*,\\s*\\'/", $p ), array( "\"' " ) ) );
              }
            }
            $value = '';
            if( isset( $data[ $field ] ) ) {
              $value = $data[ $field ];
              foreach( $functions as $name => $params ) {
                if( function_exists( $name ) ) {
                  $value = call_user_func_array( $name, array_merge( array( $value ), $params ) );
                }
                else if( method_exists( $this, 'tplFunc_' . $name ) ) {
                  $value = call_user_func_array( array( $this, 'tplFunc_' . $name ), array_merge( array( $value ), $params ) );
                }
              }
            }
            $resolved[ $subject ] = $value;
          }
        }
      }
      array_pop( self::$mCurrentRecord );
      end( self::$mCurrentRecord );
      return str_replace( array_keys( $resolved ), array_values( $resolved ), $this->mTplText );
    }

    public function tplFunc_email( $value ) {
			if ( filter_var( $value, FILTER_VALIDATE_EMAIL ) ) {
				$value = sprintf( '<a href="mailto:%s">%s</a>', $value, $value );
			}
			return $value;
    }

    public function tplFunc_ifempty( $value, $replacement ) {
      return $value ?: $replacement;
    }

    public function tplFunc_fmt( $fmtStr, $valFld ) {
			$r = self::getCurrentRecord();
			return isset( $r[ $valFld ] ) ? sprintf( $fmtStr, $r[ $valFld ] ) : '';
    }

  }

  ?>
