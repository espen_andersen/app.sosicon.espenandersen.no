<?php
  require_once( DIR_LIB . '/i_application.inc.php' );
  require_once( DIR_LIB . '/template.inc.php' );

  class AppBase implements IApplication {

    private array $mData;

    public function AppBase() {
      $this->mData = [ ];
    }

    public function run(): void {

    }

    public function tpl( string $idt, bool $output = true ): ?string {
      $t = new Template( $idt );
      $html = $t->render( $this->mData ?? [ ] );
      if( $output ) {
        echo( $html );
      }
      return $output ? null : $html;
    }

  }
