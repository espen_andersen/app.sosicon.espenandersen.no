<?php

  require_once( DIR_LIB . '/i_application.inc.php' );
  require_once( DIR_LIB . '/app_base.inc.php' );
  require_once( DIR_LIB . '/app_frontend.inc.php' );

  class Factory {

    private static ?IApplication $theApp = null;

    public static function getApplication(): IApplication {

      if( self::$theApp == null ) {

        self::$theApp = new AppBase();
        $params = explode( '/', $_GET[ 'q' ] ?? 'front' );
        switch( $params[ 0 ] ) {
          case 'api':
            self::$theApp = new AppApi( self::$theApp );
            break;
          default: // front
            self::$theApp = new AppFrontend( self::$theApp );
        }

      }
      return self::$theApp;
    }

  }
