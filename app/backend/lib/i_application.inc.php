<?php
  interface IApplication {
    public function run(): void;
    public function tpl( string $idt, bool $output ): ?string;
  }
