<?php
  require_once( DIR_LIB . '/i_application.inc.php' );
  require_once( DIR_LIB . '/template.inc.php' );

  define( 'REPORT_MAINPAGE', '20457749-4ba8-4dce-8cb5-735ff557815c/page/hrTeC' );
  define( 'REPORT_ARTICLE',  '20457749-4ba8-4dce-8cb5-735ff557815c/page/p_8t4r4byfpc' );

  class AppFrontend implements IApplication {

    private IApplication $mParent;
    private array $mData;

    public function AppFrontend( IApplication $parent ) {
      $this->mParent = $parent;
      $this->mData = [ ];
    }

    public function run(): void {
      $this->mParent->run();
      $this->mData = $this->getData();
      $this->tpl( 'main' );
    }

    public function tpl( string $idt, bool $output = true ): ?string {
      $t = new Template( $idt );
      $html = $t->render( $this->mData ?? [ ] );
      if( $output ) {
        echo( $html );
      }
      return $output ? null : $html;
    }

    private function getData(): array {
      $iframeUrl = $this->getIframeUrl();
      return [ 'iframe_src' => $iframeUrl ];
    }

    private function getIframeUrl() {
      $path = explode( '/', $_GET[ 'q' ] ?? '' );
      if( $path[ 0 ] == 'article' && preg_match( '/^\d{4,7}$/', $path[ 1 ] ?? '' ) ) {
        $params = rawurlencode( json_encode( [ 'ds0.article_id' => $path[ 1 ] ], TRUE ) );
        $reportPath = REPORT_ARTICLE . '?params=' . $params;
      }
      else {
        $reportPath = REPORT_MAINPAGE;
      }
      return 'https://datastudio.google.com/embed/reporting/' . $reportPath;
    }

  }
