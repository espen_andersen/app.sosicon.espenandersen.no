<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Sosicon online app</title>
    <meta name="description" content="Sosicon online conversion tool">
    <meta name="author" content="Espen Andersen">
    <link rel="shortcut icon" type="image/ico" href="https://app.sosicon.espenandersen.no/favicon.ico">
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.3.1/dist/jquery.min.js"></script>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oxygen+Mono">
    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
    <script src="semantic/semantic.min.js"></script>
    <script src="bundle.js"></script>
  </head>
  <body>
    <div class="content">
      <div class="ui two column grid">
        <div class="left aligned eight wide column">
          <h1><a href="https://sosicon.espenandersen.no">Sosicon</a></h1>
        </div>
        <div class="right aligned eight wide column">
          <div class="center aligned segment uploadbutton">
            <input type="file" class="inputfile" id="sosifileinput" />
            <label for="sosifileinput" class="ui button">
              <i class="ui upload icon"></i>Load SOSI
            </label>
          </div>
        </div>
      </div>
      <div class="ui one column grid">
        <p>
          Converts SOSI files from Norwegian government bodies to Shapefile.
          Please note that the conversion takes place in your browser.
          Your data is never uploaded to the server. At the main site, you can
          <a href="https://sosicon.espenandersen.no/sosicon-web-app/" title="About the web application">
          read more</a> about the application. This is free and open source software.
          By using this service, you accept the terms and conditions given in the
          <a href="https://github.com/espena/sosicon/blob/master/LICENSE.txt">License agreement</a>
          for Sosicon.
        </p>
      </div>
      <div class="ui one column grid">
        <div class="column">
          <ul id="sosicon_log"></ul>
        </div>
      </div>
      <div class="ui one column grid">
        <div class="column">
          <div id="sosicon_result" class="ui relaxed divided list"></div>
        </div>
      </div>
    </div>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-PQ1T611BYC"></script>
    <script>
      window.dataLayer = window.dataLayer || []
      function gtag(){dataLayer.push(arguments)}
      gtag('js', new Date())
      gtag('config', 'G-PQ1T611BYC', {
        custom_map: {
          dimension1: 'sosicon_shape_filecount',
          dimension2: 'sosicon_sosi_filesize',
          dimension3: 'sosicon_time_elapsed'
        }
      })
    </script>
  </body>
</html>
