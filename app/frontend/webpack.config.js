const path = require( 'path' );
const ForkTsCheckerWebpackPlugin = require( 'fork-ts-checker-webpack-plugin' );
const CopyWebpackPlugin = require( 'copy-webpack-plugin' );
const ExtraWatchWebpackPlugin = require( 'extra-watch-webpack-plugin' );

module.exports = {
  entry: './src/main.ts',
  mode: 'development',
  performance: {
    hints: false
  },
  stats: {
    children: true
  },
  output: {
    filename: 'bundle.js',
    path: path.resolve( __dirname, '../../www' ),
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(ts|js)?$/i,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-typescript",
            ],
          },
        },
      },
      {
        test: /\.scss$/i,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      },
      {
          test: /\.css$/i,
          use: [ 'style-loader', 'css-loader' ]
      },
      {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
              loader: 'babel-loader'
          }
      }
    ]
  },
  resolve: {
    extensions: [ ".ts", ".js" ]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
       { from: './static/favicon.ico' },
       { from: './static/logo.jpg' },
       { from: './static/index.php' },
       { from: './static/wasm/sosicon.js' },
       { from: './static/wasm/sosicon.wasm' },
       { from: './semantic/dist', to: 'semantic/' }
      ]
    }),
    new ForkTsCheckerWebpackPlugin(),
    new ExtraWatchWebpackPlugin({
      dirs: [ 'static' ],
      files: [ 'webpackconfig.js' ]
    })
  ]
};
