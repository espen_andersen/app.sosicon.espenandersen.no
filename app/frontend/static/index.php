<?php
  define( 'DIR_LIB', '../app/backend/lib' );
  define( 'DIR_TPL', '../app/backend/tpl' );
  require_once( DIR_LIB . '/factory.inc.php' );
  $theApp = Factory::getApplication();
  $theApp->run();
