const ctx :Worker = self as any

type SosiconAsmModule = {
  FS_createDataFile: Function,
  _malloc: Function,
  _sosicon_run: Function,
  _sosicon_load_sosi_data: Function,
  HEAPU8: {
    set: Function
  }
}

(<any> self)['sosiconPrint'] = (message :string, style :string = '') => {
  ctx.postMessage({
    action: 'log',
    message: message,
    style: style
  })
}

(<any> self)['sosiconAbort'] = (filecount :number) => {
  ctx.postMessage({
    action: 'abort'
  })
}

(<any> self)['sosiconResult'] = (filecount :number) => {
  ctx.postMessage({
    action: 'result',
    filecount: filecount
  })
}

(<any> self)['sosiconResultFile'] = (filename :string, filesize :string, fileUrl :string) => {
  ctx.postMessage({
    action: 'result_file',
    filename: filename,
    filesize: filesize,
    fileUrl: fileUrl
  })
}

class Sosicon {

  private sosiData :ArrayBuffer
  private sosiconCompiled :ArrayBuffer
  private sosiconModule :SosiconAsmModule

  constructor(sosiData :Uint8Array, moduleCompiled :ArrayBuffer) {
    this.sosiData = sosiData
    this.sosiconCompiled = moduleCompiled
  }

  build() :Promise<SosiconAsmModule> {
    (<any> self).importScripts('./sosicon.js');
    return new Promise(resolve => {
      (<any> self).SosiconModule(this.sosiconCompiled)
        .then((m :SosiconAsmModule) => {
          this.sosiconModule = m
          resolve(this.sosiconModule)
        })
    })
  }
}

ctx.addEventListener('message', e => {
  switch(e.data.action) {
    case 'load_wasm':
      new Sosicon(e.data.sosiData, e.data.moduleCompiled)
        .build()
        .then(sosiconAsmModule => {
          ctx.postMessage({action: 'input_file_loading'})
          sosiconAsmModule.FS_createDataFile('/', 'sosicon_input_data.sos', e.data.sosiData, true, true, true)
          ctx.postMessage({action: 'input_file_loaded'})
          sosiconAsmModule._sosicon_run()
        })
      break;
  }
})
