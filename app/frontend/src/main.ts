import './main.scss'

let sosiconWorker :Worker = null
let sosiData :ArrayBuffer = null

type Ga4Event = {
  event_name: string,
  sosicon_result?: string,
  sosicon_shape_filecount?: string,
  sosicon_sosi_filesize?: number,
  sosicon_time_elapsed?: number
}

$(document).ready(() => {
  const fileInput = document.getElementById('sosifileinput')
  fileInput.addEventListener('change', (e :Event) => {
    const file :File = (<HTMLInputElement> e.target).files[0];
    (<any> window).sosiconSosiFilesize = file.size
    const fr = new FileReader();
    fr.onload = () => {
      sosiData = new Uint8Array(<ArrayBufferLike>fr.result);
      sosiconWorker = new Worker(new URL('./components/sosicon/Sosicon', import.meta.url))
      WebAssembly.compileStreaming(fetch('sosicon.wasm'))
        .then(module => {
          sosiconWorker.addEventListener('message', e => {
            switch(e.data.action) {
              case 'input_file_loaded':
                sosiconClearLog();
                break;
              case 'log':
                sosiconPrint(e.data.message, e.data.style)
                break;
              case 'abort':
                sosiconGa4Event({
                  event_name: 'sosicon_abort',
                  sosicon_sosi_filesize: (<any> window).sosiconSosiFilesize,
                  sosicon_time_elapsed: Date.now() - (<any> window).sosiconTsStart
                });
                break;
              case 'result':
                sosiconGa4Event({
                  event_name: 'sosicon_success',
                  sosicon_sosi_filesize: (<any> window).sosiconSosiFilesize,
                  sosicon_shape_filecount: e.data.filecount,
                  sosicon_time_elapsed: Date.now() - (<any> window).sosiconTsStart
                });
                break;
              case 'result_file':
                sosiconAddResultFile(e.data.filename, e.data.filesize, e.data.fileUrl)
                break;
            }
          })

          sosiconGa4Event({
            event_name: 'sosicon_start'
          });

          (<any> window).sosiconTsStart = Date.now()
          sosiconWorker.postMessage({
            action: 'load_wasm',
            moduleCompiled: module,
            sosiData: sosiData
          })
        })
    }
    sosiconClearLog();
    sosiconPrint(`Reading ${file.name} ...`)
    fr.readAsArrayBuffer(file)
    document.getElementById('sosicon_log').classList.add('loader')
  })
  sosiconPrint('Ready|')
})

// https://stackoverflow.com/questions/15900485
function formatBytes(bytes :number, decimals :number = 2) {
    if (bytes === 0) {
      return '0 Bytes'
    }
    const k = 1024
    const dm = decimals < 0 ? 0 : decimals
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    const i = Math.floor(Math.log(bytes) / Math.log(k))
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
}

function descriptionFromExtension(extension :string) :string {
  let fileDescription :string = ''
  switch(extension) {
    case 'shp':
      fileDescription = '<i class="map outline icon"></i> geometry data'
      break
    case 'dbf':
      fileDescription = '<i class="list icon"></i> properties table'
      break
    case 'shx':
      fileDescription = '<i class="indent icon"></i> index file'
      break
    case 'prj':
      fileDescription = '<i class="file video outline icon"></i> projection data'
      break
  }
  return fileDescription
}

function sosiconAddResultFile(filename :string, filesize :number, fileUrl :string) :void {

  const humanFilesize = formatBytes(filesize)
  const resultList = document.getElementById('sosicon_result')
  const resultItem = document.createElement('a')
  const displayName :string = filename.replace('/sosicon_input_data_', '')
  const metaData = displayName.split(/[_\.]/)
  const fileType :string = descriptionFromExtension(metaData[2])

  if( sosiconAddResultFile.currentTema != metaData[0] ) {
    sosiconAddResultFile.currentTema = metaData[0]
    const sectionTitle = document.createElement('h3')
    sectionTitle.classList.add('item')
    sectionTitle.innerHTML = sosiconAddResultFile.currentTema
    resultList.appendChild(sectionTitle)
  }

  resultItem.classList.add('item')
  resultItem.setAttribute('href', fileUrl)
  resultItem.setAttribute('download', displayName)
  resultItem.innerHTML =
     `<i class="large download middle aligned icon"></i>
      <div class="content">
        <span class="header">${displayName}</span>
        <div class="description">
          <strong><span>${metaData[1]} &#183; file size: ${humanFilesize}</span></strong></div>
          <span class="filetype">${fileType}</span>
      </div>`

  resultList.appendChild(resultItem)
}

sosiconAddResultFile.currentTema = ''

function sosiconClearLog() :void {
  document.getElementById('sosicon_log').innerHTML = ''
}

function sosiconGa4Event(event :Ga4Event) :void {
  const { event_name, ...event_parameters } = event;
  (<any> window).gtag?.('event', event_name, event_parameters)
}

function sosiconPrint(message :string, style :string = '') :void {
  const log = document.getElementById('sosicon_log')
  if(message.indexOf('[?') > -1) {
    return
  }
  let logEntry :Element | null
  if(message.indexOf('\r') > -1) {
    logEntry = log.firstElementChild || log.insertBefore(document.createElement('li'), log.firstElementChild)
  }
  else {
    logEntry = log.insertBefore(document.createElement('li'), log.firstElementChild)
  }
  message = message.replace(/([0-9]+)/, '<em>$1</em>')
  message = message.replace(/(\|)/, '<span class="cursor">|</span>')
  logEntry.innerHTML = message.replace('\r', '')
  if(style) {
    style.split(' ').forEach(cls => logEntry.classList.add(cls))
  }
  if(message.indexOf('Processing OBJTYPEs done') > -1) {
    sosiconPrint( "Conversion finished", 'blink green' )
  }
}
