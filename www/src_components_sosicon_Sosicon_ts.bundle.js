/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./src/components/sosicon/Sosicon.ts":
/*!*******************************************!*\
  !*** ./src/components/sosicon/Sosicon.ts ***!
  \*******************************************/
/***/ (() => {

eval("function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }\n\nfunction _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, \"prototype\", { writable: false }); return Constructor; }\n\nvar ctx = self;\n\nself['sosiconPrint'] = function (message) {\n  var style = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';\n  ctx.postMessage({\n    action: 'log',\n    message: message,\n    style: style\n  });\n};\n\nself['sosiconAbort'] = function (filecount) {\n  ctx.postMessage({\n    action: 'abort'\n  });\n};\n\nself['sosiconResult'] = function (filecount) {\n  ctx.postMessage({\n    action: 'result',\n    filecount: filecount\n  });\n};\n\nself['sosiconResultFile'] = function (filename, filesize, fileUrl) {\n  ctx.postMessage({\n    action: 'result_file',\n    filename: filename,\n    filesize: filesize,\n    fileUrl: fileUrl\n  });\n};\n\nvar Sosicon = /*#__PURE__*/function () {\n  function Sosicon(sosiData, moduleCompiled) {\n    _classCallCheck(this, Sosicon);\n\n    this.sosiData = sosiData;\n    this.sosiconCompiled = moduleCompiled;\n  }\n\n  _createClass(Sosicon, [{\n    key: \"build\",\n    value: function build() {\n      var _this = this;\n\n      self.importScripts('./sosicon.js');\n      return new Promise(function (resolve) {\n        self.SosiconModule(_this.sosiconCompiled).then(function (m) {\n          _this.sosiconModule = m;\n          resolve(_this.sosiconModule);\n        });\n      });\n    }\n  }]);\n\n  return Sosicon;\n}();\n\nctx.addEventListener('message', function (e) {\n  switch (e.data.action) {\n    case 'load_wasm':\n      new Sosicon(e.data.sosiData, e.data.moduleCompiled).build().then(function (sosiconAsmModule) {\n        ctx.postMessage({\n          action: 'input_file_loading'\n        });\n        sosiconAsmModule.FS_createDataFile('/', 'sosicon_input_data.sos', e.data.sosiData, true, true, true);\n        ctx.postMessage({\n          action: 'input_file_loaded'\n        });\n\n        sosiconAsmModule._sosicon_run();\n      });\n      break;\n  }\n});\n\n//# sourceURL=webpack://app.sosicon.espenandersen.no/./src/components/sosicon/Sosicon.ts?");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/components/sosicon/Sosicon.ts"]();
/******/ 	
/******/ })()
;