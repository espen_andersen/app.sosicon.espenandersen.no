/*
 * ATTENTION: The "eval" devtool has been used (maybe by default in mode: "development").
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./node_modules/babel-loader/lib/index.js??ruleSet[1].rules[0].use!./src/worker.ts":
/*!*****************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib/index.js??ruleSet[1].rules[0].use!./src/worker.ts ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\nvar ctx = self;\nvar mod = null;\nWebAssembly.compileStreaming(fetch(\"sosicon.wasm\")).then(function (module) {\n  mod = module;\n  self.postMessage({\n    module: module\n  });\n});\nctx.addEventListener(\"message\", function (event) {\n  if (event.data.sosiData) {}\n});\n/*\n\nclass SosiconConverter {\n  run() :Array<string> {\n    return ['foo','bar']\n  }\n}\n\n*/\n\n\n\n//# sourceURL=webpack://app.sosicon.espenandersen.no/./src/worker.ts?./node_modules/babel-loader/lib/index.js??ruleSet%5B1%5D.rules%5B0%5D.use");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The require scope
/******/ 	var __webpack_require__ = {};
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./node_modules/babel-loader/lib/index.js??ruleSet[1].rules[0].use!./src/worker.ts"](0, __webpack_exports__, __webpack_require__);
/******/ 	
/******/ })()
;