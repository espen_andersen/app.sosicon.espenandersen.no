#!/bin/bash
if [ -d /etc/letsencrypt/accounts ]; then
  # Letsencrypt was here
  echo "Letsencrypt initialization OK"
else
  mv -f /root/init_tls_cert_default.conf /etc/nginx/conf.d/default.conf
  rm -f /etc/letsencrypt/*
  service nginx reload
  certbot --non-interactive --agree-tos --email post@espenandersen.no --nginx -d app.sosicon.espenandersen.no
fi
