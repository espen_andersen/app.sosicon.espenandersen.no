#!/bin/bash
APP_SERVER_PROJECTDIR="$(realpath $(dirname "$0")/..)"
docker-compose -f docker-compose_prod.yml up -d --build
docker exec -t sosicon_app_node bash -c "npm install && npx webpack --mode=production"
docker exec -t sosicon_app_nginx bash -c "rm -rf /var/nginx/cache/*"
docker-compose restart sosicon_app_nginx
cd ~/
ln -sf "$APP_SERVER_PROJECTDIR" app-server-projectdir
