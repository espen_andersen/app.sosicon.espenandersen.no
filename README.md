# app.sosicon.espenandersen.no

Application for converting SOSI files to Shapefiles in the browser. This is a WebAssembly implementation of
[sosicon](https://github.com/espena/sosicon), a command-line converter written in C++.

You can read more information on the [project site](https://sosicon.espenandersen.no/sosicon-web-app/).
